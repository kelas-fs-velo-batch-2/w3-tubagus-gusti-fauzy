<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Group::create([
            'group_name' => 'one',
            'main_salary' => 1851800,
            'allowance' => 360000,
        ]);

        Group::create([
            'group_name' => 'two',
            'main_salary' => 2399200,
            'allowance' => 490000,
        ]);

        Group::create([
            'group_name' => 'three',
            'main_salary' => 2920800,
            'allowance' => 540000,
        ]);
    }
}
