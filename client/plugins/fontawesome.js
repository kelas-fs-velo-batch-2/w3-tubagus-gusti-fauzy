import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faUser, faLock, faSignOutAlt, faCog, faSchool, faEnvelope, faEdit, faTrash
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub, faInstagram, faFacebook
} from '@fortawesome/free-brands-svg-icons'

config.autoAddCss = false

library.add(
  faUser, faLock, faSignOutAlt, faCog, faGithub, faSchool, faEnvelope, faEdit, faTrash
)

Vue.component('Fa', FontAwesomeIcon)
